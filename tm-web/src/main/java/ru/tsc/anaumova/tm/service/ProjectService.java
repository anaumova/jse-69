package ru.tsc.anaumova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.anaumova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyNameException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    public Project add(@Nullable final String name, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project(name);
        project.setUserId(userId);
        return projectRepository.save(project);
    }

    public Project save(@Nullable final Project project, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        return projectRepository.save(project);
    }

    @Transactional
    public void remove(@Nullable final Project project, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new ProjectNotFoundException();
        removeByIdAndUserId(project.getId(), userId);
    }

    @Transactional
    public void remove(@Nullable final List<Project> projects, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projects == null) throw new ProjectNotFoundException();
        projects.forEach(project -> remove(project, userId));
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteByIdAndUserId(id, userId);
    }

    @NotNull
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    public Project findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    public boolean existsByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.existsByIdAndUserId(id, userId);
    }

    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.countByUserId(userId);
    }

}