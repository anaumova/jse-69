package ru.tsc.anaumova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.api.service.IJmsService;
import ru.tsc.anaumova.tm.dto.LogDto;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class JmsComponent {

    private static final int THREAD_COUNT = 3;

    @NotNull
    @Autowired
    private IJmsService service;

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LogDto logDto = service.createMessage(object, type);
            service.send(logDto);
        });
    }

    public void stop() {
        es.shutdown();
    }

}