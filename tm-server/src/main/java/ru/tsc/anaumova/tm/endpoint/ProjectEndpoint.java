package ru.tsc.anaumova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.anaumova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.anaumova.tm.api.service.dto.IProjectDtoService;
import ru.tsc.anaumova.tm.dto.model.ProjectDto;
import ru.tsc.anaumova.tm.dto.model.SessionDto;
import ru.tsc.anaumova.tm.dto.request.*;
import ru.tsc.anaumova.tm.dto.response.*;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.anaumova.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @NotNull final ProjectDto project = getProjectService().changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Date dateBegin = request.getDateBegin();
        @Nullable final Date dateEnd = request.getDateBegin();
        getProjectService().create(userId, name, description, dateBegin, dateEnd);
        return new ProjectCreateResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<ProjectDto> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getProjectService().removeById(userId, id);
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDto project = getProjectService().findOneById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDto project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

}