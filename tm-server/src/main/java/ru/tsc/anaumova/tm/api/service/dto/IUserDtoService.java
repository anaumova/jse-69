package ru.tsc.anaumova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.dto.model.UserDto;
import ru.tsc.anaumova.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDto> {

    @NotNull
    UserDto create(@NotNull String login, @NotNull String password);

    @NotNull
    UserDto create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    UserDto create(@NotNull String login, @NotNull String password, @Nullable Role role);

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findByEmail(@NotNull String email);

    @NotNull
    void removeByLogin(@NotNull String login);

    @NotNull
    UserDto setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    UserDto updateUser(
            @NotNull String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}